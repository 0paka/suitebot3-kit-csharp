﻿using SuiteBot3.Ai;
using SuiteBot3.Game;
using SuiteBot3.Test.Json;

namespace SuiteBot3.Test
{
	[TestClass]
	public class BotRequestHandlerTest
	{
		private class DummyBotAi : IBotAi
		{
			public DummyBotAi()
			{
				InitializeAndMakeMoveCalled = false;
				MakeMoveCalled = false;
			}

			public string InitializeAndMakeMove(GameSetup gameSetup)
			{
				InitializeAndMakeMoveCalled = true;

				return "FIRSTMOVE";
			}

			public string MakeMove(GameRound gameRound)
			{
				MakeMoveCalled = true;

				return "NEXTMOVE";
			}

			public bool InitializeAndMakeMoveCalled { get; private set; }

			public bool MakeMoveCalled { get; private set; }
		}

		[TestInitialize]
		public void Setup()
		{
			BotAi = new DummyBotAi();
			RequestHandler = new BotRequestHandler(BotAi);
		}

		[TestMethod]
		public void OnMovesMessageMakeMoveShouldBeCalled()
		{
			string aiMove = RequestHandler.ProcessRequest(JsonUtilTest.MovesMessage);

			Assert.IsFalse(BotAi.InitializeAndMakeMoveCalled);

			Assert.AreEqual("NEXTMOVE", aiMove);
		}

		[TestMethod]
		public void OnSetupMessageInitializeShouldBeCalled()
		{
			string aiMove = RequestHandler.ProcessRequest(JsonUtilTest.SetupMessage);

			Assert.IsFalse(BotAi.MakeMoveCalled);

			Assert.AreEqual("FIRSTMOVE", aiMove);
		}

		[TestMethod]
		public void OnInvalidRequestNoAiMethodsShouldBeCalled()
		{
			RequestHandler.SuppressErrorLogging = true;

			RequestHandler.ProcessRequest("Invalid request");

			Assert.IsFalse(BotAi.InitializeAndMakeMoveCalled);
			Assert.IsFalse(BotAi.MakeMoveCalled);
		}

		private DummyBotAi BotAi { get; set; }

		private BotRequestHandler RequestHandler { get; set; }
	}
}
