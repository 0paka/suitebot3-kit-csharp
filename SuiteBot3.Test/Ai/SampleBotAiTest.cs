﻿using System.Collections.Generic;
using System.Linq;
using SuiteBot3.Ai;
using SuiteBot3.Game;

namespace SuiteBot3.Test.Ai
{
	[TestClass]
	public class SampleBotAiTest
	{
		private const int MaxRounds = 4;

		private const int MyId = 1;

		private const int PlanHeight = 5;
		private const int PlanWidth = 10;

		private static List<Player> Players = new List<Player> { new Player(MyId, "Me"), new Player(2, "Enemy 1"), new Player(3, "Enemy 2") };

		private static List<Point> StartingPositions = new List<Point> { new Point(1, 1), new Point(2, 2), new Point(3, 3) };

		private static GamePlan GamePlan = new GamePlan(PlanWidth, PlanHeight, StartingPositions, MaxRounds);

		private GameSetup GameSetup = new GameSetup(MyId, Players, GamePlan);

		[TestInitialize]
		public void Setup()
		{
			BotAi = new SampleBotAi();
		}

		[TestMethod]
		public void ShouldFireIfEnemyFired()
		{
			BotAi.InitializeAndMakeMove(GameSetup);

			string myMoveAfterEnemyFired = BotAi.MakeMove(MakeGameRound("MY MOVE", SampleBotAi.Move.Fire, SampleBotAi.Move.WarmUp));

			Assert.AreEqual(SampleBotAi.Move.Fire, myMoveAfterEnemyFired);
		}

		[TestMethod]
		public void ShouldNoLongerWarmUpInSecondHalf()
		{
			GameRound gameRound = MakeGameRound("MY MOVE", "ENEMY1 MOVE", "ENEMY2 MOVE");

			BotAi.InitializeAndMakeMove(GameSetup);

			BotAi.MakeMove(gameRound);

			string myRound3Move = BotAi.MakeMove(gameRound);

			Assert.AreNotEqual(SampleBotAi.Move.WarmUp, myRound3Move);

			string myRound4Move = BotAi.MakeMove(gameRound);

			Assert.AreNotEqual(SampleBotAi.Move.WarmUp, myRound4Move);
		}

		[TestMethod]
		public void ShouldNotFireIfEnemyDidNotFire()
		{
			string myFirstMove = BotAi.InitializeAndMakeMove(GameSetup);

			Assert.AreNotEqual(SampleBotAi.Move.Fire, myFirstMove);

			string myMoveAfterEnemyDidNotFire = BotAi.MakeMove(MakeGameRound("MY MOVE", SampleBotAi.Move.WarmUp, SampleBotAi.Move.WarmUp));

			Assert.AreNotEqual(SampleBotAi.Move.Fire, myMoveAfterEnemyDidNotFire);
		}

		[TestMethod]
		public void ShouldWarmUpInFirstHalf()
		{
			GameRound gameRound = MakeGameRound("MY MOVE", "ENEMY1 MOVE", "ENEMY2 MOVE");

			string myRound1Move = BotAi.InitializeAndMakeMove(GameSetup);

			Assert.AreEqual(SampleBotAi.Move.WarmUp, myRound1Move);

			string myRound2Move = BotAi.MakeMove(gameRound);

			Assert.AreEqual(SampleBotAi.Move.WarmUp, myRound2Move);
		}

		private GameRound MakeGameRound(params string[] moves)
		{
			return new GameRound(Enumerable.Range(0, Players.Count)
										   .Select(i => new Moves(Players[i].Id, moves[i]))
										   .ToList());
		}

		private IBotAi BotAi { get; set; }
	}
}
