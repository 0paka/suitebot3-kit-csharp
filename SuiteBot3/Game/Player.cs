﻿namespace SuiteBot3.Game
{
	public class Player
	{
		public Player(int id, string name)
		{
			Id = id;
			Name = name;
		}

		public override bool Equals(object obj)
		{
			if (this == obj)
				return true;

			if (obj == null || GetType() != obj.GetType())
				return false;

			var player = (Player) obj;

			return Id == player.Id;
		}

		public override int GetHashCode()
		{
			return Id;
		}

		public override string ToString()
		{
			return "Player{" +
				"id=" + Id +
				", name='" + Name + '\'' +
				'}';
		}

		public int Id { get; }

		public string Name { get; }
	}
}
