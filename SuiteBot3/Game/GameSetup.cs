﻿using System.Collections.Generic;

namespace SuiteBot3.Game
{
	public class GameSetup
	{
		public GameSetup(int aiPlayerId, IList<Player> players, GamePlan gamePlan)
		{
			AiPlayerId = aiPlayerId;
			GamePlan = gamePlan;
			Players = players;
		}

		public int AiPlayerId { get; }

		public GamePlan GamePlan { get; }
		
		public IList<Player> Players { get; }

		public static GameSetup FromGameStateDto(GameStateDto dto)
		{
			return new GameSetup(dto.AiPlayerId, null, null);
		} 
	}
}
