﻿namespace SuiteBot3.Game
{
    public interface IGameState
    {
        int GetCurrentRound();
        int GetRoundsRemaining();

        GameStateDto ToDto();
    }
    
    public class GameStateDto : IGameState
    {
        public int AiPlayerId { get; set; }
        public int CurrentRound { get; set; }
        public int RemainingRounds { get; set; }
        
        public int GetCurrentRound()
        {
            return CurrentRound;
        }

        public int GetRoundsRemaining()
        {
            return RemainingRounds;
        }

        public GameStateDto ToDto()
        {
            return this;
        }
    }
}