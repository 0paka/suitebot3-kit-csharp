﻿namespace SuiteBot3.Game
{
	public class Point
	{
		public Point(int x, int y)
		{
			X = x;
			Y = y;
		}

		public override bool Equals(object obj)
		{
			if (this == obj)
				return true;

			if (obj == null || GetType() != obj.GetType())
				return false;

			var point = (Point) obj;

			return X == point.X && Y == point.Y;
		}

		public override int GetHashCode()
		{
			var result = X;

			result = 31 * result + Y;

			return result;
		}

		public override string ToString()
		{
			return "[" + X +", " + Y + "]";
		}

		public int X { get; }

		public int Y { get; }
	}
}
