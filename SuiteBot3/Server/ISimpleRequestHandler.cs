﻿namespace SuiteBot3.Server
{
	public interface ISimpleRequestHandler
	{
		string ProcessRequest(string request);
	}
}
