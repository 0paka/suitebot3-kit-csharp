﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SuiteBot3.Game;

namespace SuiteBot3.Json
{
	public static class JsonUtil
	{
		public static GameStateDto DecodeGameState(string json)
		{
			return JsonConvert.DeserializeObject<GameStateDto>(json);
		}
	}
}
