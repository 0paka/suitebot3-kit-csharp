﻿using System;
using SuiteBot3.Ai;
using SuiteBot3.Game;
using SuiteBot3.Json;
using SuiteBot3.Server;

namespace SuiteBot3
{
	public class BotRequestHandler : ISimpleRequestHandler
	{
		public BotRequestHandler(IBotAiFactory botFactory)
		{
			BotAiFactory = botFactory;
			SuppressErrorLogging = false;
		}

		public string ProcessRequest(string request)
		{
			try
			{
				return ProcessRequestInternal(request).SerializeMoves();
			}
			catch (Exception exception)
			{
				if (SuppressErrorLogging) return exception.ToString();

				Console.Error.WriteLine("Error while processing the request: " + request);
				Console.Error.WriteLine(exception.StackTrace);

				return exception.ToString();
			}
		}

		private IMoves ProcessRequestInternal(string request)
		{
			var gameState = JsonUtil.DecodeGameState(request);

			if (gameState.GetCurrentRound() == 1)
				BotAi = BotAiFactory.CreateNewBot(GameSetup.FromGameStateDto(gameState));

			return BotAi.MakeMove(gameState);
		}

		public bool SuppressErrorLogging { get; set; }

		private IBotAiFactory BotAiFactory { get; }
		
		private IBotAi BotAi { get; set; }
	}
}
