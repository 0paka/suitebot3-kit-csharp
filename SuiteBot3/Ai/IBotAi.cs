﻿using SuiteBot3.Game;

namespace SuiteBot3.Ai
{
	public interface IBotAi
	{
		/// <summary>
		/// This method is called in each round.
		/// </summary>
		/// <param name="gameState">The current state of the game</param>
		/// <returns>The moves to play.</returns>
		IMoves MakeMove(IGameState gameState);
	}
}
