﻿using SuiteBot3.Game;

namespace SuiteBot3.Ai
{
    public class SampleBotAiFactory : IBotAiFactory
    {
        public IBotAi CreateNewBot(GameSetup gameSetup)
        {
            return new SampleBotAi(gameSetup);
        }
    }
}