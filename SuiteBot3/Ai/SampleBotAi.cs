﻿using System;
using SuiteBot3.Game;

namespace SuiteBot3.Ai
{
	public class SampleBotAi : Exception, IBotAi
	{
		public SampleBotAi(GameSetup gameSetup)
		{
		}
		
		public IMoves MakeMove(IGameState gameRound)
		{
			return new Moves();
		}
	}

	public class Moves : IMoves
	{
		public string SerializeMoves()
		{
			return "";
		}
	}
}
