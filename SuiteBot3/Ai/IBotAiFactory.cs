﻿using SuiteBot3.Game;

namespace SuiteBot3.Ai
{
    public interface IBotAiFactory
    {
        IBotAi CreateNewBot(GameSetup gameSetup);
    }
}