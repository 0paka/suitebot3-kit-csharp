﻿using System;
using System.Collections.Generic;
using SuiteBot3.Ai;
using SuiteBot3.Server;

namespace SuiteBot3
{
	public class BotServer
	{
		public const int DefaultPort = 9001;

		public static void Main(string[] args)
		{
			// Replace with your own bot implementation class.
			IBotAiFactory botAi = new SampleBotAiFactory();

			var port = DeterminePort(args);

			Console.WriteLine("Listening on port " + port);

			new SimpleServer(port, new BotRequestHandler(botAi)).Run();
		}

		private static int DeterminePort(IReadOnlyList<string> args)
		{
			return args.Count == 1 ? Convert.ToInt32(args[0]) : DefaultPort;
		}
	}
}
